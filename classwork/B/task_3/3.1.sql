-- БД «Комп. фірма». Вкажіть виробника для тих ноутбуків, що
-- мають жорсткий диск об’ємом не менше 10 Гбайт. 
-- Вивести: maker, type, speed, hd.

SELECT maker,type,speed,hd FROM product 
JOIN laptop ON product.model = laptop.model AND hd > 10
