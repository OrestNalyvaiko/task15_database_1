-- Для попереднього запиту накласти додаткову умову для фільт-
-- руванння стрічок з таблиці PC, де hd >= 10. Цю умову реа-
-- лізувати в конструкції WHERE, а потім в LEFT JOIN … ON.
-- Порівняти результати.

SELECT * FROM pc 
LEFT JOIN laptop ON pc.speed = laptop.speed WHERE pc.hd >= 10;

SELECT * FROM pc 
LEFT JOIN laptop ON pc.speed = laptop.speed AND pc.hd >= 10;