-- БД «Комп. фірма». Виконати LEFT JOIN (RIGHT JOIN та
-- FULL JOIN) для таблиць PC та Laptop. З’єднання таблиць
-- виконати за стовпцем speed.

SELECT * FROM pc 
LEFT JOIN laptop ON pc.speed = laptop.speed;

SELECT * FROM pc RIGHT 
JOIN laptop ON pc.speed = laptop.speed;

SELECT * FROM pc 
FULL JOIN laptop;
