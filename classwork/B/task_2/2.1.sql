-- БД «Кораблі». Знайти всі кораблі, імена класів яких
-- закінчуються літерою 'o'.

SELECT name FROM ships 
WHERE class RLIKE 'o$'