-- БД «Кораблі». Знайти всі кораблі, імена класів яких
-- закінчуються літерою 'o', але не на 'go'.
SELECT name FROM ships 
WHERE class RLIKE 'o$' AND class NOT RLIKE 'go%'