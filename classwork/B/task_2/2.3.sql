-- БД «Кораблі». Знайти всі кораблі, імена яких починаються на
-- 'C' або 'K' та закінчуються літерою 'a' або 'o'.

SELECT name FROM ships 
WHERE name RLIKE '^[CK]' AND name RLIKE '[ao]$' 