-- БД «Комп. фірма». Знайдіть номер моделі, швидкість та розмір
-- жорсткого диску для усіх ПК, вартість яких менше 500 дол.
-- Вивести: model, speed, hd, price.
-- Вихідні дані впорядкувати за зростанням за стовпцем speed.

SELECT model,speed,hd,price FROM PC 
WHERE price < 500 
ORDER BY speed