-- БД «Комп. фірма». Знайти виробників принтерів (без дублікатів).
-- Вивести: maker, type.
-- Вихідні дані впорядкувати за спаданням за стовпцем maker.

SELECT DISTINCT maker,type FROM product 
WHERE type = 'Printer' 
ORDER BY maker DESC  