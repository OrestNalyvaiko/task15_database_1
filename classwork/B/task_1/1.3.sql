-- БД «Комп. фірма». Знайти виробників ПК.
-- Вивести: maker, type.
-- Вихідні дані впорядкувати за спаданням за стовпцем maker.

SELECT maker,type FROM product 
WHERE type = 'PC'
ORDER BY maker DESC