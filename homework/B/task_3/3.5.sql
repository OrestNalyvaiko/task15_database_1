-- БД «Кораблі». Знайдіть країни, що мали класи як звичайних
-- бойових кораблів 'bb', так і класи крейсерів 'bc'. Вивести:
-- country, типи з класом 'bb', типи з класом 'bc'.

SELECT c1.country, c1.class AS class_bb, c2.class AS class_bc FROM classes c1
JOIN classes c2 ON c1.country = c2.country AND c1.type = 'bb' AND c2.type = 'bc'