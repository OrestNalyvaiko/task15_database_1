-- БД «Кораблі». Для кораблів таблиці Ships вивести їх
-- водотоннажність.

SELECT name, ships.class, launched, displacement FROM ships
JOIN classes ON ships.class = classes.class