-- БД «Комп. фірма». Знайдіть номер моделі та виробника
-- прінтера, яка має ціну вищу за 300 дол. Вивести: model, maker.

SELECT product.model, maker FROM product
JOIN printer ON product.model = printer.model AND price > 300