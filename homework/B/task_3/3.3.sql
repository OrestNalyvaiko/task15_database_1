-- БД «Комп. фірма». Знайдіть виробників ноутбуків з процесором не вище 500 МГц.
-- Вивести: maker.

SELECT maker FROM product
JOIN laptop ON product.model = laptop.model AND  speed <= 500