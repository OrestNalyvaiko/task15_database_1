-- БД «Комп. фірма». Знайдіть номер моделі та виробника ПК,
-- яка має ціну менше за 600 дол. Вивести: model, maker.

SELECT product.model, maker FROM product
JOIN pc ON product.model = pc.model AND price < 600