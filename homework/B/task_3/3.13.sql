-- БД «Кораблі». Для кораблів таблиці Ships вивести країни,
-- яким вони належать.

SELECT name, country FROM ships
JOIN classes ON ships.class = classes.class