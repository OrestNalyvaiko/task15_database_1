-- БД «Комп. фірма». Виведіть виробника, номер моделі та ціну
-- кожного комп’ютера, що є у БД. Вивести: maker, model, price.

SELECT maker, pc.model, price FROM pc
JOIN product ON pc.model = product.model