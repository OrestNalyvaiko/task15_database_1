-- БД «Комп. фірма». Вкажіть виробника для тих ПК, що мають
-- жорсткий диск об’ємом не більше 8 Гбайт.
-- Вивести: maker, type, speed, hd.

SELECT maker, type, speed, hd FROM product
JOIN pc ON product.model = pc.model AND hd <= 8