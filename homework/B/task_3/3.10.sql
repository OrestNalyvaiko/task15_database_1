-- БД «Комп. фірма». Виведіть виробника, тип, модель та частоту
-- процесора для ноутбуків, частота процесорів яких перевищує 600 МГц.
-- Вивести: maker, type, model, speed.

SELECT maker, type, laptop.model, speed FROM laptop
JOIN product ON laptop.model = product.model AND speed > 600