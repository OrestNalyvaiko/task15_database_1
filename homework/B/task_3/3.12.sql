-- БД «Кораблі». Для кораблів, що вціліли у битвах, вивести
-- назви та дати битв, у яких вони брали участь.

SELECT ship, name, date FROM outcomes
JOIN battles ON outcomes.battle = battles.name AND result != 'sunk'

