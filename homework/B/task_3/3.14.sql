-- БД «Аеропорт». Для рейсових літаків 'Boeing' вказати назви
-- компаній, яким вони належать.

SELECT DISTINCT name FROM trip
JOIN company ON trip.ID_comp = company.ID_comp AND plane = 'Boeing'