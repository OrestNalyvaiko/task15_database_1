-- БД «Аеропорт». Для пасажирів таблиці Passenger вивести
-- дати, коли вони користувалися послугами авіаліній.

SELECT name, date FROM passenger
JOIN pass_in_trip ON passenger.ID_psg = pass_in_trip.ID_psg 