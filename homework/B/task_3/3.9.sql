-- БД «Комп. фірма». Виведіть усі можливі моделі ПК, їх
-- виробників та ціну (якщо вона вказана).
-- Вивести: maker, model, price.

SELECT maker, pc.model, price FROM pc
JOIN product ON pc.model = product.model