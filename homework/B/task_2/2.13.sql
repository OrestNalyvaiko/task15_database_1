-- БД «Аеропорт». Вивести прізвища пасажирів (друге слово у
-- стовпці name), що не починаються на літеру 'J'.

SELECT name FROM passenger
WHERE name RLIKE '[[:blank:]][^J]'