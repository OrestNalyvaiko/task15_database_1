-- БД «Кораблі». З таблиці Ships вивести назви кораблів, що
-- починаються на 'W' та закінчуються літерою 'n'.

SELECT name FROM ships
WHERE name RLIKE '^(W).*(n)$'