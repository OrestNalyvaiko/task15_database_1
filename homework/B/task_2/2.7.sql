-- БД «Кораблі». Вивести назви битв, які складаються з двох слів
-- та друге слово не закінчується на літеру 'c'.

SELECT name FROM battles
WHERE name RLIKE '.+[[:blank:]].+([^c])$'