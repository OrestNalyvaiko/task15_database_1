-- БД «Кораблі». З таблиці Ships вивести назви кораблів та роки
-- їх спуску на воду, назва яких не закінчується на літеру 'a'.

SELECT name,launched FROM ships
WHERE name RLIKE '[^a]$'