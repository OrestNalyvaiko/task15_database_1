-- БД «Кораблі». З таблиці Ships вивести назви кораблів, що
-- мають у своїй назві дві літери 'e'.

SELECT name FROM ships
WHERE name RLIKE 'e.*e'
