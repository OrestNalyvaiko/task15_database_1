-- БД «Аеропорт». З таблиці Trip вивести інформацію про рейси,
-- що прилітають в інтервалі часу між 17 та 23 годинами включно.

SELECT * FROM trip
WHERE hour(time_in) BETWEEN 17 AND 22 OR (hour(time_in) = 23 AND minute(time_in) = 0 AND second(time_in) = 0) 