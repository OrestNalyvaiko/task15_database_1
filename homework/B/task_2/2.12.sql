-- БД «Аеропорт». Вивести прізвища пасажирів (друге слово у
-- стовпці name), що починаються на літеру 'С'.

SELECT name FROM passenger
WHERE name RLIKE '[[:blank:]]C'
