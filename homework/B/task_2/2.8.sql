-- БД «Аеропорт». З таблиці Trip вивести інформацію про рейси,
-- що вилітають в інтервалі часу між 12 та 17 годинами включно.

SELECT * FROM trip
WHERE hour(time_out) BETWEEN 12 AND 16  OR (hour(time_out) = 17 AND minute(time_out) = 0 AND second(time_out) = 0)