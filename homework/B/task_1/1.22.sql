-- БД «Кораблі». Вивести усі потоплені кораблі.
-- Вивести: ship, battle, result.
-- Вихідні дані впорядкувати за спаданням за стовпцем ship.

SELECT * FROM outcomes
WHERE result = 'sunk'
ORDER BY ship DESC