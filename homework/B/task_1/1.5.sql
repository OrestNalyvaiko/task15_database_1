-- БД «Кораблі». Перерахувати назви головних кораблів (з
-- таблиці Ships). Вивести: name, class.
-- Вихідні дані впорядкувати за зростанням за стовпцем name.

SELECT name, class FROM ships
WHERE name = class
ORDER BY name