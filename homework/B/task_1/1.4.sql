-- БД «Комп. фірма». Знайдіть номер моделі, швидкість та розмір
-- диску ПК, що мають CD-приводи зі швидкістю '12х' чи '24х' та
-- ціну меншу 600 дол. Вивести: model, speed, hd, cd, price.
-- Вихідні дані впорядкувати за спаданням за стовпцем speed.

SELECT model, speed, hd, cd, price FROM pc 
WHERE (cd = '12px' OR '24px') AND price < 600 
ORDER BY speed DESC