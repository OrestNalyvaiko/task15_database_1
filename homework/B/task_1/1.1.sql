-- БД «Комп. фірма». Знайти виробників ноутбуків. 
-- Вивести: maker, type. Вихідні дані впорядкувати за зростанням за стовпцем maker.
SELECT maker, type FROM product 
WHERE type = 'Laptop' 
ORDER BY maker