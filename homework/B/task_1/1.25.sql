-- БД «Аеропорт». Знайдіть номера усіх рейсів, на яких курсує літак 'TU-134'.
-- Вивести: trip_no, plane, town_from, town_to.
-- Вихідні дані впорядкувати за спаданням за стовпцем time_out.

SELECT trip_no, plane, town_from, town_to FROM trip
WHERE plane = 'TU-134'
ORDER BY time_out DESC