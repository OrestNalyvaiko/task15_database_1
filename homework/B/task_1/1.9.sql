-- БД «Комп. фірма». Знайти модель, частоту процесора та об’єм
-- жорсткого диску для тих комп’ютерів, що комплектуються
-- накопичувачами 10 або 20 Мб та випускаються виробником 'A'.
-- Вивести: model, speed, hd. 
-- Вихідні дані впорядкувати за зростанням за стовпцем speed.

SELECT maker, pc.model, speed, hd FROM pc
JOIN product ON pc.model = product.model AND maker = 'A' AND (hd = 10 OR hd = 20)
ORDER BY speed
